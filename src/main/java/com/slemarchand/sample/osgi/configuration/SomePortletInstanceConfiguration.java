package com.slemarchand.sample.osgi.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;


@ExtendedObjectClassDefinition(
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
	
@Meta.OCD(
	id = "com.slemarchand.osgi.configuration.sample.SomePortletInstanceConfiguration"
)
public interface SomePortletInstanceConfiguration {
	
	@Meta.AD(deflt = "200", name = "max-item-count", required = false)
	public int cacheSize();
	
	@Meta.AD(deflt = "ASC", name = "direction", required = false)
	public String direction();

}