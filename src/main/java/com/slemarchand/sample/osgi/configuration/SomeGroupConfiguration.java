package com.slemarchand.sample.osgi.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;


@ExtendedObjectClassDefinition(
		scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(
	id = "com.slemarchand.osgi.configuration.sample.SomeGroupConfiguration"
)
public interface SomeGroupConfiguration {
	

	@Meta.AD(
		name = "feature-enabled", required = true
	)
	public boolean featureEnabled();
}