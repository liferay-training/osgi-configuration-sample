package com.slemarchand.sample.osgi.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;


@ExtendedObjectClassDefinition(
		scope = ExtendedObjectClassDefinition.Scope.SYSTEM)
@Meta.OCD(
	id = "com.slemarchand.osgi.configuration.sample.SomeSystemConfiguration"
)
public interface SomeSystemConfiguration {
	
	@Meta.AD(deflt = "5000", name = "cache-size", required = false)
	public int cacheSize();

	@Meta.AD(
		deflt = "none",
		name = "provider", required = false
	)
	public String provider();
}