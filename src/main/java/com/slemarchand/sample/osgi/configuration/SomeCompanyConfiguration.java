package com.slemarchand.sample.osgi.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;


@ExtendedObjectClassDefinition(
		scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(
	id = "com.slemarchand.osgi.configuration.sample.SomeCompanyConfiguration"
)
public interface SomeCompanyConfiguration {
	
	@Meta.AD(
		deflt = "",
		name = "columns", required = false
	)
	public String[] columns();
}